import React from "react"

const BankList = () =>{
    const ListAdds =[
        {
            bankName: "Paga Todo",
            description: "Banco Paga Todo es Para Todos",
            age: 10,
            url: "http://www.one.url.com",
        },
        {
            bankName: "BBVA Bancomer",
            description: "BBVA Bancomer Creando Oportunidades",
            age: 10,
            url: "http://www.one.url.com",
        },
        {
            bankName: "Paga Todo",
            description: "Banco Paga Todo es Para Todos",
            age: 10,
            url: "http://www.one.url.com"
        },
        {
            bankName: "BBVA Bancomer",
            description: "BBVA Bancomer Creando Oportunidades",
            age: 10,
            url: "http://www.one.url.com"
        },
        {
            bankName: "Paga Todo",
            description: "Banco Paga Todo es Para Todos",
            age: 10,
            url: "http://www.one.url.com"
        },
        {
            bankName: "BBVA Bancomer",
            description: "BBVA Bancomer Creando Oportunidades",
            age: 10,
            url: "http://www.one.url.com"
        },
    ] 

    return(
        <div className="">
            <ul>
                { ListAdds.map( item => {
                    return(
                        <div className="bg-white shadow overflow-hidden rounded-md">
                            <ul>
                                <li>
                                    <a href="#" className="block hover:bg-gray-200 focus:outline-none focus:bg-gray-60 duration-150 ease-in-out">
                                        <div className="px-4 py-4 sm:px-6">
                                            <div className="flex items-center justify-between">
                                                <div className="font-medium text-green-500 truncate">
                                                    {`banckName: ${item.bankName}` }
                                                </div>
                                            </div>
                                            <div className="mt-2 font-medium">
                                                { `description: ${item.description}` }
                                            </div>
                                            <div className="mt-2 font-medium">
                                                { `age: ${item.age}` }
                                            </div>
                                            <div className="mt-2 font-medium">
                                                { `url: ${item.url}` }
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    );
                })
                }
            </ul>
        </div>
    )
}

export default BankList